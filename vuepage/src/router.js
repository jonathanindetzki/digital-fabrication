import Vue from 'vue'
import Router from 'vue-router'
import Home from './pages/home/Home.vue'

Vue.use(Router)

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './pages/about/About.vue')
    },
    {
      path: '/tasks',
      name: 'tasks',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "tasks" */ './pages/tasks/Tasks.vue')
    },
    {
      path: '/tasks/week2',
      name: 'week2',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "week2" */ './pages/tasks/week2/index.vue')
    },
    {
      path: '/tasks/week3',
      name: 'week3',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "week3" */ './pages/tasks/week3/index.vue')
    },
    {
      path: '/tasks/week4',
      name: 'week4',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "week4" */ './pages/tasks/week4/index.vue')
    },
    {
      path: '/tasks/week5',
      name: 'week5',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "week5" */ './pages/tasks/week5/index.vue')
    },
    {
      path: '/tasks/week6',
      name: 'week6',
      component: () => import(/* webpackChunkName: "week6" */ './pages/tasks/week6/index.vue')
    },
    {
      path: '/tasks/week7',
      name: 'week7',
      component: () => import(/* webpackChunkName: "week7" */ './pages/tasks/week7/index.vue')
    },
    {
      path: '/tasks/week8',
      name: 'week8',
      component: () => import(/* webpackChunkName: "week8" */ './pages/tasks/week8/index.vue')
    },
    {
      path: '/tasks/week9',
      name: 'week9',
      component: () => import(/* webpackChunkName: "week9" */ './pages/tasks/week9/index.vue')
    },
    {
      path: '/tasks/final',
      name: 'Final Project',
      component: () => import(/* webpackChunkName: "final" */ './pages/tasks/final/index.vue')
    }
  ]
})
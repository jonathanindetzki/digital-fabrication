import PrismHighlight from './component/prism-highlight.vue';

export default {
    install(Vue) {
        Vue.component('prism-highlight', PrismHighlight)
    }
}
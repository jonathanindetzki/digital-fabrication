module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/digital-fabrication/'
    : '/',

  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.(md|txt)$/i,
          use: [
            {
              loader: 'file-loader',
            },
          ],
        },
      ],
    },
  },
}